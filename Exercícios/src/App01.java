import java.util.Scanner;

/**
 * 
 * @author 2016100088
 * @version 1.0
 */
public class App01 {

	public static void main(String[] args) {
		
	try {

		// declara��o de vari�veis
		double av1, av2, av3, media;
		
		Scanner teclado = new Scanner(System.in);
		
		
		//Entrada de Dados
		System.out.println("Digite a nota AV1: ");
		av1 = teclado.nextDouble();
		
		System.out.println("Digite a nota AV2: ");
		av2 = teclado.nextDouble();
		
		System.out.println("Digite a nota AV3: ");
		av3 = teclado.nextDouble();
		
		//Processamento
		media = (av1+av2+av3)/3;
		
		if (media >= 7) {
			System.out.println("Aprovado com M�dia: " + media);
		} else {
			System.out.println("Reprovado com M�dia: " + media);
		}
		
		//Sa�da
		System.out.println("M�dia: " + media);
		
	}catch{
		
	}

}
	