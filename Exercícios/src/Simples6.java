import java.util.Scanner;

public class Simples6 {

	public static void main(String[] args) {
		double odom_i=0, odom_f=0, litros=0, valor_t=0, media=0, lucro=0, gasol_l=1.90;

		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Marcacao inicial do odometro (km): ");
		odom_i = teclado.nextDouble();
		
		System.out.println("Marcacao final do odometro (km): ");
		odom_f = teclado.nextDouble();
		
		System.out.println("Quantidade de combustivel gasto: (litros)");
		litros = teclado.nextDouble();
		
		System.out.println("Valor total recebido (R$): ");
		valor_t = teclado.nextDouble();
		
		media = (odom_f - odom_i) / litros;
		lucro = valor_t - (litros * gasol_l);
		
		System.out.println("Media de consumo em Km/L: " + media);
		System.out.println("Lucro (liquido) do dia: R$" + lucro);	

	}

}