import java.util.Scanner;

public class Simples2 {

	public static void main(String[] args) {
		
		float temp_f=0, temp_c=0;
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Informe a temperatura em gruas Fahrenheit ");
		temp_f = teclado.nextFloat();
		
		temp_c = ((temp_f - 32)*5)/9;
		
		System.out.println("A temperatura em graus Celsius e: " + temp_c);	
	
	}

}