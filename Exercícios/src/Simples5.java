import java.util.Scanner;

public class Simples5 {

	public static void main(String[] args) {
		float comp=0, larg=0, alt=0, area=0;
		int caixas=0;
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Qual o comprimento da cozinha? ");
		comp = teclado.nextFloat();
		
		System.out.println("Qual a largura da cozinha? ");
		larg = teclado.nextFloat();
		
		System.out.println("Qual a altura da cozinha? ");
		alt = teclado.nextFloat();
		
		area = (comp*alt*2) + (larg*alt*2);
		caixas = Math.round(area/1.5); 

        System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: " + caixas);

	}

	
	
	
}