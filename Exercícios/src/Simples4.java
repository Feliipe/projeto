import java.util.Scanner;
import java.lang.Math;

public class Simples4 {

	public static void main(String[] args) {
		float pot_lamp, larg_com, comp_com, area_com, pot_total;
		int num_lamp;
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Qual a potencia da lampada (em watts)? ");
		pot_lamp = teclado.nextFloat();
		
		System.out.println("Qual a largura do comodo (em metros)? ");
		larg_com = teclado.nextFloat();
		
		System.out.println("Qual o comprimento do comodo (em metros)? ");
		comp_com = teclado.nextFloat();
		
		area_com = larg_com * comp_com;
		pot_total = area_com * 18;
		num_lamp = Math.round(pot_total/pot_lamp); 
		
		System.out.println("Numero de lampadas necessarias para iluminar esse comodo: " + num_lamp);

	}

	
}