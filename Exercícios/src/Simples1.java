import java.util.Scanner;
import java.lang.Math;

public class Simples1 {

	public static void main(String[] args) {
		double raio=0, area=0;
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Informe o raio do circulo: ");
		raio = teclado.nextDouble();
		
		area = 3.14 * Math.sqrt(raio);
		
		System.out.println("A area do circulo e: " + area);
		
	}

}